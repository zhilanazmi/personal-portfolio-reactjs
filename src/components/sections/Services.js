import React from "react";
import { Link } from "react-scroll";
import Pagetitle from "../elements/Pagetitle";
import Service from "../elements/Service";

const servicesData = [
  {
    id: 1,
    icon: "images/bootstrap.svg",
    title: "Bootstrap",
    content:
      "Bootstrap is a free and open-source CSS framework directed at responsive, mobile-first front-end web development.",
    color: "#6C6CE5",
    contentColor: "light",
  },
  {
    id: 2,
    icon: "images/vuejs.svg",
    title: "Vue JS",
    content:
      "Vue.js is an open-source model–view-viewmodel front end JavaScript framework for building user interfaces and single-page applications ",
    color: "#F97B8B",
    contentColor: "light",
  },
  {
    id: 3,
    icon: "images/reactjs.svg",
    title: "React JS",
    content:
      "React is a free and open-source front-end JavaScript library for building user interfaces based on UI components",
    color: "#F97B8B",
    contentColor: "light",
  },
];

function Services() {
  return (
    <section id="services">
      <div className="container">
        <Pagetitle title="Services" />
        <div className="row fix-spacing">
          {servicesData.map((service) => (
            <div className="col-md-4" key={service.id}>
              <Service service={service} />
            </div>
          ))}
        </div>
        <div className="mt-5 text-center">
          <p className="mb-0">
            Looking for a custom job?{" "}
            <Link
              className="colorpink pointer"
              to="section-contact"
              spy={true}
              smooth={true}
              duration={500}
            >
              Click here
            </Link>{" "}
            to contact me! 👋
          </p>
        </div>
      </div>
    </section>
  );
}

export default Services;
